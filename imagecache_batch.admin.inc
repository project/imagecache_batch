<?php


/**
 * @file
 * Batch the selected ImageCache presets at cron run
 */

/**
 * Admin settings form
 */
function imagecache_batch_admin_settings() {
  $form = array();
  
  $form['presets'] = array(
    '#type' => 'fieldset',
    '#title' => t('Preset settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['presets']['imagecache_batch_presets'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Presets'),
    '#description' => t('Select the ImageCache presets you want to batch'),
    '#options' => _imagecache_batch_get_presets(),
    '#default_value' => variable_get('imagecache_batch_presets', array()),
  );
  
  /*$form['cron'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cron settings'),
  );
  
  $form['cron']['imagecache_batch_frequency'] = array(
    '#type' => 'radios',
    '#title' => t('Frequency'),
    '#description' => t('Select the frequency for batching the images'),
    '#options' => array(
      'hour' => t('Every hour'),
      'day' => t('Every day'),
      'week' => t('Every week'),
    ),
    '#default_value' => variable_get('imagecache_batch_frequency', 'hour'),
  );
  
  $form['cron']['hour'] = array(
    '#type' => 'fieldset',
    '#title' => t('Hour settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['cron']['hour']['imagecache_batch_hour_minute'] = array(
    '#type' => 'select',
    '#title' => t('Run at'),
    '#options' => array(
      0 => t('On the hour'),
      15 => t('15th minute'),
      30 => t('Half hour'),
      45 => t('45th minute'),
    ),
    '#default_value' => variable_get('imagecache_batch_hour_minute', 0),
  );
  
  $form['cron']['day'] = array(
    '#type' => 'fieldset',
    '#title' => t('Day settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['cron']['day']['imagecache_batch_hour_hour'] = array(
    '#type' => 'textfield',
    '#title' => t('Hour'),
    '#description' => t('Enter the hour you want the cron to run at. Format HH:MM'),
    '#default_value' => variable_get('imagecache_batch_hour_hour', ''),
    '#size' => 10,
  );
  
  $form['cron']['week'] = array(
    '#type' => 'fieldset',
    '#title' => t('Week settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['cron']['week']['imagecache_batch_week_day'] = array(
    '#type' => 'radios',
    '#title' => t('Day of the week'),
    '#options' => array(
      0 => t('Sunday'),
      1 => t('Monday'),
      2 => t('Tuesday'),
      3 => t('Wednesday'),
      4 => t('Thursday'),
      5 => t('Friday'),
      6 => t('Saterday'),
    ),
    '#default_value' => variable_get('imagecache_batch_week_day', 0),
  );
  
  $form['cron']['week']['imagecache_batch_week_hour'] = array(
    '#type' => 'textfield',
    '#title' => t('Hour'),
    '#description' => t('Enter the hour you want the cron to run at. Format HH:MM'),
    '#default_value' => variable_get('imagecache_batch_week_hour', ''),
    '#size' => 10,
  );
  */
  
  return system_settings_form($form);
}

/**
 * Internal to get the presets as options
 */
function _imagecache_batch_get_presets() {
  $options = array();
  
  foreach (imagecache_presets() as $preset) {
    $options[$preset['presetid']] = $preset['presetname'];
  }
  
  return $options;
}